package com.example.qgx.androidkeshe.Activity;

import android.content.Intent;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.qgx.androidkeshe.Model.GoodsItem;
import com.example.qgx.androidkeshe.R;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.qgx.androidkeshe.Url.Url.serverurl;

public class DianCan extends AppCompatActivity {
    private Button back = null;
    private String result = null;
    private String name = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dian_can);
        back = findViewById(R.id.back);
         result = getIntent().getStringExtra("list");
         name = getIntent().getStringExtra("name");
        Log.i("1234999", "onCreate: "+name);
        Log.i("1234999", "onCreate: "+result);
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        Looper.prepare();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(serverurl + "servlet/addOrder?shopName=" + name + "&xiangqing=" + result)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            String result = response.body().string();
            if (response.isSuccessful()) {
                if (result == null) {
                    Toast.makeText(DianCan.this, "服务器故障", Toast.LENGTH_SHORT).show();
                } else if (result.equals("1")) {
                    Toast.makeText(DianCan.this, "下单成功", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(DianCan.this, "下单失败", Toast.LENGTH_SHORT).show();
                }
            }
        } catch(IOException e){
            e.printStackTrace();
        }
                        Looper.loop();
                    }
                }

        ). start();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("123", "onCreate: "+result);
                Intent i=new Intent();
                i.setClass(DianCan.this, Main2Activity.class);
                i.putExtra("id",2);
                startActivity(i);
                finish();
            }
        });
//        Intent intent = getIntent();
//        List<GoodsItem> goods = (List<GoodsItem>) getIntent().getSerializableExtra(
//                "selectedList");
//        for (GoodsItem a : goods){
//            Log.i("123", "onCreate: "+a.name);
//        }

    }
}

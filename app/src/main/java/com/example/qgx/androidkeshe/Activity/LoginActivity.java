package com.example.qgx.androidkeshe.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.qgx.androidkeshe.R;

/**
 * Created by qgx on 2018/6/20.
 */

public class LoginActivity extends AppCompatActivity {
    //声明控件
    private Button login = null;
    private Button regist = null;
    View relativeLayout = null;
    private EditText userNmae = null;
    private EditText passWord = null;
    private String userNameString = null;
    private String passWordString = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //关联控件
        initview();

        click();

    }
//
//    private void login() {
//        new Thread(
//                new Runnable() {
//                    @Override
//                    public void run() {
//                        Looper.prepare();
//                        try {
//                            OkHttpClient client = new OkHttpClient();
//                            Request request = new Request.Builder()
//                                    .url(serverurl + "servlet/Login?userName=" + userNameString + "&passWord=" + passWordString)
//                                    .build();
//                            Response response = null;
//                            response = client.newCall(request).execute();
//                            String result = response.body().string();
//                            if (response.isSuccessful()) {
//                                if (result == null) {
//                                    Toast.makeText(LoginActivity.this, "服务器故障", Toast.LENGTH_SHORT).show();
//                                } else if (result.equals("0")) {
//                                    Toast.makeText(LoginActivity.this, "登录失败，没有此用户", Toast.LENGTH_SHORT).show();
//                                } else if (result.equals("2")) {
//                                    Toast.makeText(LoginActivity.this, "密码输入错误,请检查用户名/密码", Toast.LENGTH_SHORT).show();
//                                } else if (result.equals("1")) {
//                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                                    intent.putExtra("userName", userNameString);
//                                    intent.putExtra("flag", "0");
//                                    startActivity(intent);
//                                }
//                            }
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        Looper.loop();
//                    }
//                }
//        ).start();
//    }

    /**
     * 初始化视图
     */
    private void initview() {
        relativeLayout = findViewById(R.id.login_container);
        login = (Button) findViewById(R.id.login);
        regist = (Button) findViewById(R.id.regist);
        userNmae = (EditText) findViewById(R.id.username_edit_text);
        passWord = (EditText) findViewById(R.id.password_edit_text);
    }

    /**
     * 登录、注册设置监听
     */
    private void click() {
        //登录按钮监听器
        login.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        userNameString = userNmae.getText().toString().trim();
                        passWordString = passWord.getText().toString().trim();
                        if (userNameString == null || userNameString.equals("")) {
                            Toast.makeText(LoginActivity.this, "请输入用户名", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (passWordString == null || passWordString.equals("")) {
                            Toast.makeText(LoginActivity.this, "请输入密码", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        //login();
                        Intent intent = new Intent(LoginActivity.this, Main2Activity.class);
                        intent.putExtra("userName", userNameString);
                        intent.putExtra("passWord", passWordString);
                        startActivity(intent);
                        finish();
                    }
                });
        //注册按钮监听器
        regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegistActivity.class);
                startActivity(intent);
            }
        });

    }
}

package com.example.qgx.androidkeshe.Activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.qgx.androidkeshe.Fragement.Fragment1;
import com.example.qgx.androidkeshe.Fragement.FragmentManagerHelper;
import com.example.qgx.androidkeshe.Fragement.MyFragment;
import com.example.qgx.androidkeshe.Fragement.OrderFragment;
import com.example.qgx.androidkeshe.Fragement.ShopFragment;
import com.example.qgx.androidkeshe.R;
import com.example.qgx.androidkeshe.networkbroadcastreceiver.receiver.NetworkReceiver;
import com.example.qgx.androidkeshe.networkbroadcastreceiver.tools.NetStatusUtil;

import java.util.ArrayList;
import java.util.List;

import cn.qqtheme.framework.picker.OptionPicker;
import cn.qqtheme.framework.picker.SinglePicker;
import cn.qqtheme.framework.widget.WheelView;

public class Main2Activity extends AppCompatActivity implements NetworkReceiver.NetStatusInterface {
    private NetworkReceiver mMyBroadcastReceiver;
    private FrameLayout frameLayout = null;
    private FragmentManagerHelper mFragmentHelper;
    private Fragment1 fragment1;
    private MyFragment myFragment;
    private OrderFragment orderFragment;
    private ShopFragment shopFragment;
    private  String mima= null;
    private  String username= null;

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    if (shopFragment == null) {
                        shopFragment = new ShopFragment();
                    }
                    mFragmentHelper.switchFragmnet(shopFragment);
                    return true;
                case R.id.navigation_dashboard:
                    if (orderFragment == null) {
                        orderFragment = new OrderFragment();
                    }
                    mFragmentHelper.switchFragmnet(orderFragment);
                    return true;
                case R.id.navigation_notifications:
                    if (myFragment == null) {
                        myFragment = new MyFragment();
                    }
                    mFragmentHelper.switchFragmnet(myFragment);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onResume() {
        registerMyReceiver();
        int id = getIntent().getIntExtra("id", 0);
        if (id == 1) {
            findViewById(R.id.navigation_home).performClick();
        }
        if (id == 2) {
            findViewById(R.id.navigation_dashboard).performClick();
        }
        if (id == 3) {
            findViewById(R.id.navigation_notifications).performClick();
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unRegisterMyReceiver();
    }
    /**
     * 注销
     */
    public void unRegisterMyReceiver() {
        if (mMyBroadcastReceiver != null) {
            unregisterReceiver(mMyBroadcastReceiver);
        }
    }
    /**
     * 注册
     */
    public void registerMyReceiver() {
        if (mMyBroadcastReceiver == null) {
            mMyBroadcastReceiver = new NetworkReceiver();
        }
        mMyBroadcastReceiver.setNetStateInterface(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(mMyBroadcastReceiver, filter);
    }
    public void netChangeListener(int status) {
        switch (status) {
            case NetStatusUtil.NETWORK_WIFI:

                Toast.makeText(this, "当前网络类型是WIFi", Toast.LENGTH_SHORT).show();
                break;
            case NetStatusUtil.NETWORK_MOBILE:
                Toast.makeText(this, "当前网络类型是移动网络", Toast.LENGTH_SHORT).show();
                break;
            case NetStatusUtil.NETWORK_NONE:

                Toast.makeText(this, "当前没有网络", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        frameLayout = findViewById(R.id.content2);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        mFragmentHelper = new FragmentManagerHelper(getSupportFragmentManager(), R.id.content2);
        mima=getIntent().getStringExtra("passWord");
        username =getIntent().getStringExtra("userName");
        Log.i("123", "onCreate: "+mima);
        shopFragment = new ShopFragment();
        mFragmentHelper.add(shopFragment);

    }

    /**
     * 我的信息
     *
     * @param view
     */
    public void line1(View view) {
        //findViewById(R.id.navigation_dashboard).performClick();
        Intent intent =new Intent(Main2Activity.this,MyActivity.class);
        startActivity(intent);
    }

    /**
     * 我的订单
     *
     * @param view
     */
    public void line2(View view) {
        findViewById(R.id.navigation_dashboard).performClick();
    }

    public void line3(View view) {
      Toast.makeText(Main2Activity.this,"尚未开发",Toast.LENGTH_SHORT).show();
    }

    public void line4(View view) {
        Toast.makeText(Main2Activity.this,"尚未开发",Toast.LENGTH_SHORT).show();
    }

    /**
     * 修改密码
     *
     * @param view
     */
    public void line5(View view) {
        Intent intent = new Intent(Main2Activity.this,XiuGaiMiMaActivity.class);
        intent.putExtra("oldPassWord",mima);
        intent.putExtra("name",username);
        startActivity(intent);

    }

    /**
     * 注销登录
     *
     * @param view
     */
    public void line6(View view) {


        Intent intent = new Intent(Main2Activity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }


    public void select(View view) {
        OptionPicker picker = new OptionPicker(this, new String[]{
                "第一项", "第二项", "第三项", "第四项", "第五项", "第六项", "第七项",
                "这是一个很长很长很长很长很长很长很长很长很长的很长很长的很长很长的项"
        });
        picker.setCanceledOnTouchOutside(false);
        picker.setDividerRatio(WheelView.DividerConfig.FILL);
        picker.setShadowColor(Color.RED, 40);
        picker.setSelectedIndex(1);
        picker.setCycleDisable(true);
        picker.setTextSize(11);
        picker.setOnOptionPickListener(new OptionPicker.OnOptionPickListener() {
            @Override
            public void onOptionPicked(int index, String item) {
                //showToast("index=" + index + ", item=" + item);


                Toast.makeText(Main2Activity.this, "index=" + index + ", item=" + item, Toast.LENGTH_SHORT).show();
            }
        });
        picker.show();
    }

    //重写onKeyDown方法
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        //判断当点击的是返回键
        if (keyCode == event.KEYCODE_BACK) {
            exit();//退出方法
        }
        return true;
    }

    private long time = 0;

    //退出方法
    private void exit() {
        //如果在两秒大于2秒
        if (System.currentTimeMillis() - time > 2000) {
            //获得当前的时间
            time = System.currentTimeMillis();
            Toast.makeText(this, "再点击一次退出应用程序", Toast.LENGTH_SHORT).show();
        } else {
            System.exit(0);
        }
    }

}

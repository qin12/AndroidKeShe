package com.example.qgx.androidkeshe.Activity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;

import com.example.qgx.androidkeshe.R;
import com.roger.match.library.MatchButton;
import com.roger.match.library.MatchTextView;

import tyrantgit.explosionfield.ExplosionField;

public class MyActivity extends Activity {
    private Button button = null;
    private Button back = null;
    private MatchTextView mMatchTextView;
    MatchButton matchButton;
    private ExplosionField mExplosionField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        button = findViewById(R.id.reset3);
        back = findViewById(R.id.back3);
       back.setOnClickListener(
        new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        }
);
       mMatchTextView = (MatchTextView)findViewById(R.id.matchtv);
         matchButton = (MatchButton)findViewById(R.id.matchbt);

        matchButton.setTextColor(Color.BLACK);




        mExplosionField = ExplosionField.attach2Window(this);
        addListener(findViewById(R.id.activity_my));
        button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onCreate(null);
                    }
                }
        );
    }

    private void addListener(View root) {
        if (root instanceof ViewGroup) {
            ViewGroup parent = (ViewGroup) root;
            for (int i = 0; i < parent.getChildCount(); i++) {
                addListener(parent.getChildAt(i));
            }
        } else {
            root.setClickable(true);
            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mExplosionField.explode(v);
                    v.setOnClickListener(null);
                }
            });
        }
    }

}

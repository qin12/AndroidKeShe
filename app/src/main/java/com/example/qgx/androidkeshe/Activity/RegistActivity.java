package com.example.qgx.androidkeshe.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.qgx.androidkeshe.R;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.qgx.androidkeshe.Url.Url.serverurl;

/**
 *
 * Created by qgx on 2018/6/20.
 */

public class RegistActivity extends AppCompatActivity {
    private Button regist = null;
    private Button reset = null;
    private View relativeLayout = null;
    private EditText userNmae = null;
    private EditText passWord = null;
    private EditText againPassWord = null;
    private String userNmaeString;
    private String passWordString;
    private String againPassWordSrring;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist);
        initView();

        reset.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        userNmae.setText("");
                        passWord.setText("");
                        againPassWord.setText("");
                    }
                }
        );
        //注册按钮监听器
        regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //判断两次密码是否一样
                userNmaeString = userNmae.getText().toString().trim();
                passWordString = passWord.getText().toString().trim();
                againPassWordSrring = againPassWord.getText().toString();

                if (userNmaeString == null || userNmaeString.equals("")) {
                    Toast.makeText(RegistActivity.this, "请输入用户名", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (passWordString == null || passWordString.equals("")) {
                    Toast.makeText(RegistActivity.this, "请输入密码", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (againPassWordSrring == null || againPassWordSrring.equals("")) {
                    Toast.makeText(RegistActivity.this, "请再次输入密码", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!passWordString.equals(againPassWordSrring)) {
                    Toast.makeText(RegistActivity.this, "两次填入的密码不同", Toast.LENGTH_SHORT).show();
                    return;
                }
                regist();
            }
        });

    }

    private void regist() {
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        Looper.prepare();
                        OkHttpClient client = new OkHttpClient();
                        Request request = new Request.Builder()
                                .url(serverurl + "servlet/Regist?userName=" + userNmaeString + "&passWord=" + passWordString)
                                .build();
                        Response response = null;
                        try {
                            response = client.newCall(request).execute();
                            String result = response.body().string();
                            if (response.isSuccessful()) {
                                if (result == null) {
                                    Toast.makeText(RegistActivity.this, "服务器故障", Toast.LENGTH_SHORT).show();
                                } else if (result.equals("0")) {
                                    Toast.makeText(RegistActivity.this, "注册失败,未知错误", Toast.LENGTH_SHORT).show();
                                } else if (result.equals("2")) {
                                    Toast.makeText(RegistActivity.this, "注册失败,此用户名已被注册", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(RegistActivity.this, "注册成功", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(RegistActivity.this, LoginActivity.class);
                                    intent.putExtra("userName", userNmaeString);
                                    intent.putExtra("flag", "0");
                                    startActivity(intent);
                                }
                            }
                            } catch(IOException e){
                                e.printStackTrace();
                            }
                            Looper.loop();
                        }
                    }

        ).

                    start();
                }



    private void initView() {
        relativeLayout = findViewById(R.id.regist_container);
        regist = (Button) findViewById(R.id.regist);
        reset = (Button) findViewById(R.id.reset);
        userNmae = (EditText) findViewById(R.id.username_edit_text);
        passWord = (EditText) findViewById(R.id.password_edit_text);
        againPassWord = (EditText) findViewById(R.id.again_password_edit_text);
    }
}

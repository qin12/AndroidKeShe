package com.example.qgx.androidkeshe.Activity;

import android.content.Intent;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.qgx.androidkeshe.Fragement.ShopFragment;
import com.example.qgx.androidkeshe.Fragement.OrderFragment;
import com.example.qgx.androidkeshe.Fragement.MyFragment;
import com.example.qgx.androidkeshe.BottomBar.BottomBar;
import com.example.qgx.androidkeshe.R;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.qgx.androidkeshe.Url.Url.serverurl;

public class XiuGaiMiMaActivity extends AppCompatActivity {
    private Button xiugai = null;
    private Button reset = null;
    private EditText userNmae = null;
    private EditText oldpassWord = null;
    private EditText newPassWord = null;
    private String userNmaeString;
    private String oldpassWordString;
    private String mima;
    private String name;
    private String newPassWordSrring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mima = getIntent().getStringExtra("oldPassWord");
        name = getIntent().getStringExtra("name");
        initView();
        reset.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        userNmae.setText(name);
                        oldpassWord.setText("");
                        newPassWord.setText("");
                    }
                }
        );
        //修改按钮监听器
        xiugai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userNmaeString = name;
                oldpassWordString = oldpassWord.getText().toString().trim();
                newPassWordSrring = newPassWord.getText().toString();

                if (userNmaeString == null || userNmaeString.equals("")) {
                    Toast.makeText(XiuGaiMiMaActivity.this, "请输入用户名", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (oldpassWordString == null || oldpassWordString.equals("")) {
                    Toast.makeText(XiuGaiMiMaActivity.this, "请输入旧密码", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (newPassWordSrring == null || newPassWordSrring.equals("")) {
                    Toast.makeText(XiuGaiMiMaActivity.this, "请输入新密码", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!oldpassWordString.equals(mima)) {
                    Toast.makeText(XiuGaiMiMaActivity.this, "旧密码错误", Toast.LENGTH_SHORT).show();
                    return;
                }

                xiugaimima();

            }
        });

    }

    private void xiugaimima() {
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        Looper.prepare();
                        OkHttpClient client = new OkHttpClient();
                        Request request = new Request.Builder()
                                .url(serverurl + "servlet/XiuGaiMiMa?userName="+userNmaeString+"&newpassWord="+newPassWordSrring)
                                .build();
                        Response response = null;
                        try {
                            response = client.newCall(request).execute();
                            String result = response.body().string();
                            if (response.isSuccessful()) {
                                if (result == null) {
                                    Toast.makeText(XiuGaiMiMaActivity.this, "服务器故障", Toast.LENGTH_SHORT).show();

                                } else if (result.equals("1")) {
                                    Toast.makeText(XiuGaiMiMaActivity.this, "修改密码成功", Toast.LENGTH_SHORT).show();
                                   Intent intent = new Intent(XiuGaiMiMaActivity.this,LoginActivity.class);
                                   startActivity(intent);
                                   finish();
                                } else{
                                    Toast.makeText(XiuGaiMiMaActivity.this, "修改密码失败", Toast.LENGTH_SHORT).show();

                                }
                            }
                        } catch(IOException e){
                            e.printStackTrace();
                        }
                        Looper.loop();
                    }
                }

        ).start();


    }

    private void initView() {
        xiugai = (Button) findViewById(R.id.xiugai);
        reset = (Button) findViewById(R.id.reset2);
        userNmae = (EditText) findViewById(R.id.username_edit_text2);
        userNmae.setText(name);
        oldpassWord = (EditText) findViewById(R.id.old_password_edit_text);
        newPassWord = (EditText) findViewById(R.id.new_password_edit_text);
    }
}

package com.example.qgx.androidkeshe.Fragement;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.example.qgx.androidkeshe.Activity.Main2Activity;
import com.example.qgx.androidkeshe.Model.Shop;
import com.example.qgx.androidkeshe.R;
import com.loopj.android.image.SmartImageView;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.qgx.androidkeshe.Url.Url.serverurl;

/**
 *
 *
 *
 * Created by qgx on 2018/6/22.
 */

public class Fragment1 extends LazyLoadFragment {
    private ListView lvShopss;
    private List<Shop> list;
    private TextView shop_name;
    private TextView shop_description;
    private Shop shop;
    private SmartImageView siv_shop;
    private Handler handler = null;
    @Override
    protected int setContentView() {
        handler = new Handler();
        return R.layout.fragment1;
    }

    @Override
    public void initView() {
        lvShopss = (ListView) getActivity().findViewById(R.id.shops);

    }

    @Override
    public void setListener() {

    }

    @Override
    public void initData() {
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        OkHttpClient client = new OkHttpClient();
                        Request request = new Request.Builder()
                                .url(serverurl + "servlet/ShopServlet")
                                .build();
                        client.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {

                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                final String res = response.body().string();
                                Log.i("rrr", "onResponse: " + res);
                                list =  JSON.parseArray(res, Shop.class);
                                Log.i("222", "onResponse: "+list.get(0).getImg());
                                Log.i("222", "onResponse: "+list.size());
                                handler.post(runnableUi);

                            }
                        });
                    }
                }
        ).start();

    }

    @Override
    protected void lazyLoad() {
        String message = "Fragment1" + (isInit ? "已经初始并已经显示给用户可以加载数据" : "没有初始化不能加载数据")+">>>>>>>>>>>>>>>>>>>";
        Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();

    }
    Runnable   runnableUi=new  Runnable(){
        @Override
        public void run() {
            //更新界面
            lvShopss.setAdapter(new ShopAdapter());
            Log.i("99999", "run: "+list.size());
        }

    };
    private class ShopAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = View.inflate(getActivity(), R.layout.shops_item, null);
            siv_shop = (SmartImageView) view1.findViewById(R.id.siv_icon);
            shop_name = (TextView) view1.findViewById(R.id.shop_name);
            shop_description = (TextView) view1.findViewById(R.id.shop_description);
            shop = list.get(i);
            siv_shop.setImageUrl(shop.getImg(), R.mipmap.ic_launcher, R.mipmap.ic_launcher);
            shop_name.setText(shop.getName());
            shop_description.setText(shop.getLocation());
            return view1;
        }
    }
}

package com.example.qgx.androidkeshe.Fragement;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import java.util.List;

/**
 *
 * Created by qgx on 2018/6/22.
 */

public class FragmentManagerHelper {
    private FragmentManager mFragmentManager;
    private int mContainerViewId;

    /**
     *
     * @param mFragmentManager  管理类
     * @param mContainerViewId 容器布局的id
     */
    public FragmentManagerHelper(FragmentManager mFragmentManager, int mContainerViewId) {
        this.mFragmentManager = mFragmentManager;
        this.mContainerViewId = mContainerViewId;
    }
    /**
     * 添加fragment
     * @param fragment
     */
    public void add(Fragment fragment){
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

        fragmentTransaction.add(mContainerViewId,fragment);
        fragmentTransaction.commit();
    }

    /**
     * 切换显示fragment
     * @param fragment
     */
    public void switchFragmnet(Fragment fragment) {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        List<Fragment> childFragments = mFragmentManager.getFragments();
        for (Fragment childFragment : childFragments) {
            fragmentTransaction.hide(childFragment);
        }
        if (!childFragments.contains(fragment)) {
            fragmentTransaction.add(mContainerViewId, fragment);
        } else {
            fragmentTransaction.show(fragment);
        }

        fragmentTransaction.commit();
    }
    }

package com.example.qgx.androidkeshe.Fragement;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.example.qgx.androidkeshe.Activity.ShoppingCartActivity;
import com.example.qgx.androidkeshe.Model.Order;
import com.example.qgx.androidkeshe.Model.Shop;
import com.example.qgx.androidkeshe.R;
import com.loopj.android.image.SmartImageView;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.qgx.androidkeshe.Url.Url.serverurl;


public class OrderFragment extends Fragment {
    private ListView orders;
    private List<Order> list;
    private TextView order_name;
    private TextView order_xiangqing;
    private Order order;
    private SmartImageView oiv_order;
    private Handler handler = null;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        handler =  new Handler();
        Log.i("rrr1", "run: "+"qingqiu1");

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment2, container, false);


      return view;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        orders  = getActivity().findViewById(R.id.orders);
        fillData1();
        super.onActivityCreated(savedInstanceState);
    }


    private void fillData1() {

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url(serverurl+"servlet/ShowOrders")
                        .build();
                Log.i("rrr1", "run: "+"qingqiu2");
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {

                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        final String res1 = response.body().string();
   Log.i("rrr1", "onResponse: " + res1);
                        list =  JSON.parseArray(res1, Order.class);
                        handler.post(runnableUi1);
                    }
        }
        );
    }
    Runnable   runnableUi1=new  Runnable(){
        @Override
        public void run() {
            //更新界面
            Log.i("rrr1", "run: "+"qingqiu3");
            orders.setAdapter(new OrderAdapter());


        }

    };
    private class OrderAdapter extends BaseAdapter{
        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = View.inflate(getActivity(), R.layout.orders_item, null);
            order_name =view1.findViewById(R.id.order_name);
            order_xiangqing = view1.findViewById(R.id.order_xiangqing);
            oiv_order = (SmartImageView) view1.findViewById(R.id.oiv_icon);
            order= list.get(i);
            order_name.setText(order.getName());
            order_xiangqing.setText(order.getXiangqing());
            oiv_order.setImageUrl(order.getImg(), R.mipmap.ic_launcher, R.mipmap.ic_launcher);

            return view1;
        }
    }
}

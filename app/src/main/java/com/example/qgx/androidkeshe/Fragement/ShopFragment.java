package com.example.qgx.androidkeshe.Fragement;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.qgx.androidkeshe.Activity.LoginActivity;
import com.example.qgx.androidkeshe.Activity.Main2Activity;
import com.example.qgx.androidkeshe.Activity.ShoppingCartActivity;
import com.example.qgx.androidkeshe.Model.Shop;
import com.example.qgx.androidkeshe.R;
import com.loopj.android.image.SmartImageView;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.qgx.androidkeshe.Url.Url.serverurl;

public class ShopFragment extends Fragment {
    private ListView lvShopss;
    private List<Shop> list;
    private TextView shop_name;
    private TextView shop_description;
    private Shop shop;
    private SmartImageView siv_shop;
    private Handler handler = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        handler = new Handler();
        return LayoutInflater.from(getActivity()).inflate(R.layout.fragment1, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        lvShopss = (ListView) getActivity().findViewById(R.id.shops);
        fillData();
        super.onActivityCreated(savedInstanceState);
    }

    //渲染数据
    private void fillData() {
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        OkHttpClient client = new OkHttpClient();
                        Request request = new Request.Builder()
                                .url(serverurl + "servlet/ShopServlet")
                                .build();
                        client.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {

                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                final String res = response.body().string();
                                Log.i("rrr", "onResponse: " + res);
                               list =  JSON.parseArray(res, Shop.class);
                                Log.i("222", "onResponse: "+list.get(0).getImg());
                                Log.i("222", "onResponse: "+list.size());
                                handler.post(runnableUi);


                            }
                        });
                    }
                }
        ).start();

    }
    Runnable   runnableUi=new  Runnable(){
        @Override
        public void run() {
            //更新界面

            lvShopss.setAdapter(new ShopAdapter());
            lvShopss.setOnItemClickListener(
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Intent intent = new Intent(getActivity(), ShoppingCartActivity.class);
                            intent.putExtra("name",list.get(i).getName());
                            startActivity(intent);

                        }
                    }
            );

            Log.i("99999", "run: "+list.size());
        }

    };
    private class ShopAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }


        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = View.inflate(getActivity(), R.layout.shops_item, null);
            siv_shop = (SmartImageView) view1.findViewById(R.id.siv_icon);
            shop_name = (TextView) view1.findViewById(R.id.shop_name);
            shop_description = (TextView) view1.findViewById(R.id.shop_description);
            shop = list.get(i);
            siv_shop.setImageUrl(shop.getImg(), R.mipmap.ic_launcher, R.mipmap.ic_launcher);
            shop_name.setText(shop.getName());
            shop_description.setText(shop.getLocation());
            return view1;
        }
    }
}
